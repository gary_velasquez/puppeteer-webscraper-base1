const puppeteer = require('puppeteer');

const test = async () => {
  const browser = await puppeteer.launch({headless: false, ignoreHTTPSErrors: true});
  const page = await browser.newPage();
  await page.setDefaultNavigationTimeout(60000);
  /*
  page.on("dialog", (dialog) => {
    console.log("dialog");
    dialog.dismiss();
  });
  */
  var Pokemon = [
    "Bulbasaur",
    "Ivysaur",
    "Venusaur",
    "Charmander",
    "Charmeleon",
    "Charizard",
    "Squirtle",
    "Wartortle",
    "Blastoise",
    "Caterpie",
    "Metapod",
    "Butterfree",
    "Weedle",
    "Kakuna",
    "Beedrill",
    "Pidgey",
    "Pidgeotto",
    "Pidgeot",
    "Rattata",
    "Raticate",
    "Spearow",
    "Fearow",
    "Ekans",
    "Arbok",
    "Pikachu",
    "Raichu",
    "Sandshrew",
    "Sandslash",
    "Nidoran",
    "Nidorina",
    "Nidoqueen",
    "Nidoran",
    "Nidorino",
    "Nidoking",
    "Clefairy",
    "Clefable",
    "Vulpix",
    "Ninetales",
    "Jigglypuff",
    "Wigglytuff",
    "Zubat",
    "Golbat",
    "Oddish",
    "Gloom",
    "Vileplume",
    "Paras",
    "Parasect",
    "Venonat",
    "Venomoth",
    "Diglett",
    "Dugtrio",
    "Meowth",
    "Persian",
    "Psyduck",
    "Golduck",
    "Mankey",
    "Primeape",
    "Growlithe",
    "Arcanine",
    "Poliwag",
    "Poliwhirl",
    "Poliwrath",
    "Abra",
    "Kadabra",
    "Alakazam",
    "Machop",
    "Machoke",
    "Machamp",
    "Bellsprout",
    "Weepinbell",
    "Victreebel",
    "Tentacool",
    "Tentacruel",
    "Geodude",
    "Graveler",
    "Golem",
    "Ponyta",
    "Rapidash",
    "Slowpoke",
    "Slowbro",
    "Magnemite",
    "Magneton",
    "Farfetch'd",
    "Doduo",
    "Dodrio",
    "Seel",
    "Dewgong",
    "Grimer",
    "Muk",
    "Shellder",
    "Cloyster",
    "Gastly",
    "Haunter",
    "Gengar",
    "Onix",
    "Drowzee",
    "Hypno",
    "Krabby",
    "Kingler",
    "Voltorb",
    "Electrode",
    "Exeggcute",
    "Exeggutor",
    "Cubone",
    "Marowak",
    "Hitmonlee",
    "Hitmonchan",
    "Lickitung",
    "Koffing",
    "Weezing",
    "Rhyhorn",
    "Rhydon",
    "Chansey",
    "Tangela",
    "Kangaskhan",
    "Horsea",
    "Seadra",
    "Goldeen",
    "Seaking",
    "Staryu",
    "Starmie",
    "Mr. Mime",
    "Scyther",
    "Jynx",
    "Electabuzz",
    "Magmar",
    "Pinsir",
    "Tauros",
    "Magikarp",
    "Gyarados",
    "Lapras",
    "Ditto",
    "Eevee",
    "Vaporeon",
    "Jolteon",
    "Flareon",
    "Porygon",
    "Omanyte",
    "Omastar",
    "Kabuto",
    "Kabutops",
    "Aerodactyl",
    "Snorlax",
    "Articuno",
    "Zapdos",
    "Moltres",
    "Dratini",
    "Dragonair",
    "Dragonite",
    "Mewtwo",
    "Mew"
  ];

  await page.goto('https://gearoid.me/pokemon/', {waitUntil: 'networkidle0'});
  await page.setViewport({width: 1920, height: 1080});
  await page.click('#gen2');
  await page.click('#gen3');
  await page.click('#gen4');
  await page.click('#gen5');
  await page.click('#gen6');
  await page.click('#gen7');

  await page.click('#diff2');
  await page.click('#spelling-true');

  await page.waitForSelector('#giveAnswer');
  await page.click('#giveAnswer');

  for (let poke of Pokemon) {

    await page.waitForSelector('#pokemonGuess');
    await page.focus('#pokemonGuess');
    await page.keyboard.type(poke);
    await page.click("#pokemonGuess", {clickCount: 3});
  }

  console.log('aaa');
}

test();

/*
const test = async (tabs) => {
  const pagesArray = [];
  const browser = await puppeteer.launch({headless: false, ignoreHTTPSErrors: true});
  for (var i = 0; i < tabs; i++) {
    pagesArray.push(await browser.newPage());
  }
  console.log(pagesArray.length);

  const testArray = [];

  for (let page of pagesArray) {
    testArray.push(page.goto('https://www.google.cl', {waitUntil: 'networkidle0'}));
  }

  Promise.all([testArray]);

  console.log('ok');
}

test(3);
*/
